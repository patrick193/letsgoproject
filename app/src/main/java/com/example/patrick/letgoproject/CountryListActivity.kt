package com.example.patrick.letgoproject

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_country_list.*
import java.util.*
import kotlin.collections.ArrayList

class CountryListActivity : AppCompatActivity() {

    private var list = ArrayList<Country>()

    companion object {
        const val EXTRA_KEY_FROM: Int = 1
        const val EXTRA_KEY_TO: Int = 2
        const val REQUEST_CODE = "requestCode"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_list)

        setList()
        countryList.adapter = CountryAdapter(this, list)
        countryList?.onItemSelectedListener =object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedItem = parent?.getItemAtPosition(position) as Country
                val returnIntent = Intent()
                val code = intent.getIntExtra(REQUEST_CODE, EXTRA_KEY_FROM)
                returnIntent.putExtra(CountryListActivity.REQUEST_CODE, selectedItem)

                setResult(code, returnIntent)
                finish()
            }

        }
    }

    fun setList() {
        if (this.list.size == 0) {
            val isoCountries = Locale.getISOCountries()
            var country: String
            isoCountries.forEach { iso ->
                val locale = Locale("en", iso)
                country = locale.displayCountry
                if (country.length > 0) {
                    this.list.add(Country(iso, country, ""))
                }
            }
        }
    }

    /**
     * Country adapter for default list view
     */
//    inner class CountryAdapter(context: Context, private var countryList: ArrayList<Country>) : BaseAdapter() {
//        private var context: Context? = context
//
//        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
//            val view: View?
//            val vh: ViewHolder
//
//            if (convertView == null) {
//                view = layoutInflater.inflate(R.layout.country, parent, false)
//                vh = ViewHolder(view)
//                view.tag = vh
//            } else {
//                view = convertView
//                vh = view.tag as ViewHolder
//            }
//
//            vh.name.text = countryList[position].name
//            vh.iso.text = countryList[position].id
//
//            return view
//        }
//
//        override fun getItem(position: Int): Any {
//            return this.countryList[position]
//        }
//
//        override fun getItemId(position: Int): Long {
//            return position.toLong()
//        }
//
//        override fun getCount(): Int {
//            return this.countryList.size
//        }
//    }
//
//    /**
//     * for using findById
//     */
//    private class ViewHolder(view: View?) {
//        val name: TextView = view?.findViewById<TextView>(R.id.name) as TextView
//        val iso: TextView = view?.findViewById<TextView>(R.id.iso) as TextView
//
//    }
}
