package com.example.patrick.letgoproject

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    val TAG: String = "MainActivity_"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        from.setOnClickListener {v ->
            callCountryList(v, CountryListActivity.EXTRA_KEY_FROM)
        }

        to.setOnClickListener{ v ->
            callCountryList(v, CountryListActivity.EXTRA_KEY_TO)
        }

        goBtn.setOnClickListener { view: View ->
            Log.d(TAG, "setOnclickListener")
            Toast.makeText(this, "Cool", Toast.LENGTH_LONG)
                    .show()
        }
    }

    fun callCountryList(v: View, code: Int) {
        val intent = Intent(v.context, CountryListActivity::class.java)
        intent.putExtra(CountryListActivity.REQUEST_CODE, code)
        startActivityForResult(intent, code)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val country = data?.getSerializableExtra(CountryListActivity.REQUEST_CODE) as Country
        val name = country.name
        if (requestCode == CountryListActivity.EXTRA_KEY_FROM) {
            from.text = name
        } else {
            to.text = name

        }

    }
}
