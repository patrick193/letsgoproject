package com.example.patrick.letgoproject

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class ConvertorActivity : AppCompatActivity() {

    val TAG: String = "ConvertorActivity_"
    companion object {
        const val EXTRA_KEY_FROM = "from"
        const val EXTRA_KEY_TO = "to"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_convertor)
        if (savedInstanceState == null) {
           val from: String? = if (intent.hasExtra(EXTRA_KEY_FROM)) intent.getStringExtra(EXTRA_KEY_FROM) else null
            val to: String? = if (intent.hasExtra(EXTRA_KEY_TO)) intent.getStringExtra(EXTRA_KEY_TO) else null

            Log.d(TAG, "From: " + from + " to: " + to)
        }
    }
}
