package com.example.patrick.letgoproject

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class CountryAdapter(context: Context, private var countryList: ArrayList<Country>) : BaseAdapter() {
    private val context: Context? = context

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        var inflater: LayoutInflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        view = inflater.inflate(R.layout.country, parent, false)
        var textView = view.findViewById<TextView>(R.id.name)
        var imgView = view.findViewById<ImageView>(R.id.imgViewFlag)
        textView.text = countryList[position].name
        imgView.setImageResource(context.resources.getIdentifier("drawable/" + countryList[position].id?.toLowerCase(),
                null, context.packageName))

        return view
    }

    override fun getItem(position: Int): Any {
        return this.countryList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return this.countryList.size
    }
}
